#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[])
{
    const int N = 1000000;
    char /*file_name[15],*/ extension[5]=".txt";
    int counter = 1,anotherCounter = 0;
    double sumTime = 0;
    double t, velocity = 0, x = 0, returnTime = 0, newTime = 0, interval = 2;
    double srvc[3];
    double *tArray, *velArray, *xArray;
    double vInit = 0, xInit = 5; //vzyat' mnogo
    //add an output file
    FILE *f;
    f = fopen("5.dat", "r+");
    FILE *outfile;
    outfile = fopen("outpoop.txt", "w");
    FILE *svc;
    svc = fopen("service.txt", "w");
    tArray = (double *)malloc(N*sizeof(double));
    velArray = (double *)malloc(N*sizeof(double));
    xArray = (double *)malloc(N*sizeof(double));
    if (f!=NULL)
    {
        for(double v = vInit-interval; v < vInit+interval; v+=0.1)
        {
            for(double x = xInit-interval; x < xInit+interval; x+=0.1)
            {
                char* file_name = (char*)calloc(15, sizeof(char*));
                sprintf(file_name,"v=%lf_x=%lf",v,x);
                strcat(file_name,extension);
                outfile=fopen(file_name, "w");
            //printf("entering the first IF! May the %lf\t%lf be with you!\n", v,x);
                //fprintf(outfile, "Extracting values from a source file...\n");
        for(int i = 0; i < N; i++)
        {
            fscanf(f, "%lf %lf %lf %lf %lf %lf \n", &tArray[i], &velArray[i], &xArray[i], &srvc[0], &srvc[1], &srvc[2]);
        }
        for(double halfInterval = 1; halfInterval >0.01; halfInterval-=0.01) //среднее для каждого интервала, интервалы в разные файлы
        {
            newTime = 0;
            returnTime = 0;
            for(int i = 0; i < N; i++)
            {
                //printf("entering the first IF! May the %lf be with you!\n", v);
                t = tArray[i];
                 if ((velArray[i] >= (v-halfInterval))&&(velArray[i] <= (v+halfInterval)))
                {
                    //fprintf(svc, "entering the second IF");
                    if ((xArray[i] >= (x-halfInterval))&&(xArray[i] <= (x+halfInterval)))
                    {
                        //fprintf(svc, "entering the third IF");
                        returnTime = t - newTime;
                        if (returnTime > 0)
                        {
                            newTime = t;
                            sumTime += returnTime;
                            anotherCounter++;
                            //fprintf(outfile, "Trajectory returned after %1.50f \n", returnTime);
                        }
                        /*else
                        {
                            newTime = t;
                        }*/

                    }

                }
            }

            fprintf(outfile, "%lf %lf\n" , halfInterval, sumTime/anotherCounter);
            if((sumTime/anotherCounter)>0.01)
            {
                fprintf(svc,"%lf\t%lf\t%lf\n", x, v, halfInterval);
                halfInterval = 0;
            }
            counter++;
            sumTime = 0;
            anotherCounter = 0;
            
        }
            fclose(outfile);
            free(file_name);
            }

        }


    }else
    {
        printf("Everything failed. You're dead now, Jimmy\n");
        return 0;
    }
    
    fclose(svc);
    fclose(f);
    free(tArray);
    free(velArray);
    free(xArray);
    return 0;
}